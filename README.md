#Enhanced-maven-resource-filter
##Features
It's like Default Maven Resource Filter, while the different between the default is that enhanced guy had been extended.

before introduce enhanced guy, we will recall what maven resource filter settings.
```
<resources>
    <!--The first resource set defines the files to be filtered
    and the other resource set defines the files to copy unaltered-->
    <resource>
        <directory>src/main/resources</directory>
        <filtering>true</filtering>
        <includes>
            <include>**/*.xml</include>
        </includes>
        <excludes>
            <exclude>dev/**</exclude>
        </excludes>
    </resource>
    <resource>
        <directory>src/main/resources</directory>
        <filtering>false</filtering>
        <excludes>
            <exclude>**/*.xml</exclude>
            <exclude>dev/**</exclude>
        </excludes>
    </resource>
</resources>

<filters>
    <filter>src/main/resources/environments/*.properties</filter>
</filters>
```
maven resource filter is awesome when combined with `profiles` ,  there are many blogs telling you why , here is one of it [使用 profile 和 filtering 实现多种环境下的资源配置管理](http://archboy.org/2012/05/21/apache-maven-profile-filtering-multiple-build-environments/)

However in development, we just want to manage our `.properties` files separately in different folders like below:
- dev
  - jdbc.properties
  - thrift.properies
  - and so on
- beta
  - jdbc.properties
  - thrift.properties
  - and so on
  
and we want to create as many `properties` files as we developing. but when you want to gathering all project maven settings as a super-pom, problems comes. As maven `filter` tag must be specified as a file. `src/main/resources/*.properies` or `src/main/resources` are illegal.
However since version 2.5, Maven provided way to custom resources filter([Custom resource filters](https://maven.apache.org/plugins/maven-resources-plugin/examples/custom-resource-filters.html)). Then Enhanced guy was created.
##How to use
#### maven
```
    <groupId>net.oschina.bilbodai.maven.filter</groupId>
    <artifactId>enhance-maven-resources-filter</artifactId>
    <version>1.0.0</version>
```
####pom settings
```
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-resources-plugin</artifactId>
                <version>2.7</version>
                <configuration>
                    <encoding>UTF-8</encoding>
                    <mavenFilteringHints>
                        <mavenFilteringHint>enhancedResourceFilter</mavenFilteringHint>
                    </mavenFilteringHints>
                </configuration>
                <dependencies>
                    <dependency>
                        <groupId>net.oschina.bilbodai.maven.filter</groupId>
                        <artifactId>enhance-maven-resources-filter</artifactId>
                        <version>1.0.0</version>
                    </dependency>
                </dependencies>
            </plugin>
```
**Notice**:
- `<mavenFilteringHint>enhancedResourceFilter</mavenFilteringHint>` is essential and `enhancedResourceFilter` is an identity.
- Add dependency configuration inner plugin `dependencies` tag

After that done, write your `profiles` as
```
    <profiles>
        <profile>
            <id>dev</id>
            <properties>
                <enhanced.filter.properties.dir>src/main/resources/dev,src/main/resources/devextra</enhanced.filter.properties.dir>
            </properties>
        </profile>
    </profiles>
```

**Notice**:
- property `enhanced.filter.properties.dir` is crucial. put all necessary folders divided by `,`or`;`or `single blank` .

####At Last
not forget to open filter mode:
```
        <resources>
            <resource>
                <directory>src/main/resources</directory>
                <filtering>true</filtering>
            </resource>
        </resources>
```
and enjoy it!

##Change Log
### version 1.0.1
added some feature in `<enhanced.filter.properties.dir>src/main/resources/dev,src/main/resources/devextra</enhanced.filter.properties.dir>` 
- `src/main/resources/` or `src/main/resources/*` stands for just searching `.properties` files in current directory.
- `src/main/resources/**`  stands for searching `.properties` files in current directory and all sub-directories.